import 'package:flutix/services/services.dart';
import 'package:flutix/ui/pages/pages.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

import 'bloc/blocs.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider.value(
      value: AuthServices.userStream,
      child: MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (_) => PageBloc(),
            ),
            BlocProvider(
              create: (_) => UserBloc(),
            ),
            BlocProvider(
              create: (_) => TicketBloc(),
            ),
            BlocProvider(
              create: (_) => MovieBloc()..add(FetchMovie()),
            )
          ],
          child:
              MaterialApp(debugShowCheckedModeBanner: false, home: Wrapper())),
    );
  }
}
