part of 'pages.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int bottomNavigationIndex;
  PageController pageController;

  @override
  void initState() {
    super.initState();

    bottomNavigationIndex = 0;
    pageController = PageController(initialPage: bottomNavigationIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            color: accent1,
          ),
          SafeArea(
            child: Container(
              color: Colors.white,
            ),
          ),
          PageView(
            controller: pageController,
            onPageChanged: (index){
              setState(() {
                bottomNavigationIndex = index;
              });
            },
            children: [
              MoviePage(),
              TicketPage()
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: ClipPath(
              clipper: BottomNavigationClipper(),
              child: Container(
                height: 70,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20))),
                child: BottomNavigationBar(
                  elevation: 0,
                  onTap: (index) {
                    setState(() {
                      bottomNavigationIndex = index;
                      pageController.jumpToPage(index);
                    });
                  },
                  items: [
                    BottomNavigationBarItem(
                      title: Text(
                        "Terbaru",
                        style: GoogleFonts.raleway(fontSize: 13),
                      ),
                      icon: Container(
                        margin: EdgeInsets.only(bottom: 6),
                        height: 20,
                        child: Image.asset((bottomNavigationIndex == 0)
                            ? "assets/ic_movies.png"
                            : "assets/ic_movies_grey.png"),
                      ),
                    ),
                    BottomNavigationBarItem(
                      title: Text(
                        "Tiket Saya",
                        style: GoogleFonts.raleway(fontSize: 13),
                      ),
                      icon: Container(
                        margin: EdgeInsets.only(bottom: 6),
                        height: 20,
                        child: Image.asset((bottomNavigationIndex == 1)
                            ? "assets/ic_tickets.png"
                            : "assets/ic_tickets_grey.png"),
                      ),
                    ),
                  ],
                  selectedItemColor: mainColor,
                  unselectedItemColor: Colors.grey,
                  backgroundColor: Colors.transparent,
                  currentIndex: bottomNavigationIndex,
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 46,
              width: 46,
              margin: EdgeInsets.only(bottom: 42),
              child: FloatingActionButton(
                backgroundColor: mainColor,
                elevation: 0,
                onPressed: () {
                  context.bloc<UserBloc>().add(SignOut());
                  AuthServices.signOut();
                },
                child: SizedBox(
                  height: 26,
                  width: 26,
                  child: Icon(
                    MdiIcons.walletPlus,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class BottomNavigationClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(size.width / 2 - 28, 0);
    path.quadraticBezierTo(size.width / 2 - 28, 33, size.width / 2, 33);
    path.quadraticBezierTo(size.width / 2 + 28, 33, size.width / 2 + 28, 0);
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
