part of 'pages.dart';

class SuccessPage extends StatelessWidget {
  final Ticket ticket;
  final Transactions transactions;

  const SuccessPage(this.ticket, this.transactions);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          return;
        },
        child: Scaffold(
            body: FutureBuilder(
          future:
              ticket != null ? processingTicket(context) : processingTopUp(),
          builder: (_, snapshot) =>
              (snapshot.connectionState == ConnectionState.done
                  ? Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: 150,
                            height: 150,
                            margin: EdgeInsets.only(bottom: 70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(ticket != null
                                      ? "assets/ill_tickets.png"
                                      : "assets/ill_top_up.png")),
                            ),
                          ),
                          Text((ticket != null) ? "Happy Watching" : "All Sets",
                              style: blackTextFont.copyWith(fontSize: 20)),
                          SizedBox(
                            height: 16,
                          ),
                          Text(
                              (ticket != null)
                                  ? "You have successfully\nbought the tickets"
                                  : "You have successfully\ntop up your wallet",
                              style: blackTextFont.copyWith(fontSize: 16),
                              textAlign: TextAlign.center),
                          Container(
                            height: 45,
                            width: 250,
                            margin: EdgeInsets.only(top: 70, bottom: 20),
                            child: RaisedButton(
                              elevation: 0,
                              color: mainColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              child: Text(
                                  (ticket != null) ? "My Ticket" : "My Wallet",
                                  style: whiteTextFont.copyWith(fontSize: 16)),
                              onPressed: () {},
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("Want to watch others?",
                                  style: grayTextFont.copyWith(
                                      fontWeight: FontWeight.w400)),
                              GestureDetector(
                                onTap: () {
                                  context.bloc<PageBloc>().add(GoToMainPage());
                                },
                                child: Text("Back home", style: purpleTextFont),
                              )
                            ],
                          )
                        ],
                      ),
                    )
                  : Center(
                      child: SpinKitFadingCircle(
                        size: 50,
                        color: mainColor,
                      ),
                    )),
        )));
  }

  Future<void> processingTicket(BuildContext context) async {
    context.bloc<UserBloc>().add(Purchase(ticket.totalPrice));
    context.bloc<TicketBloc>().add(BuyTickets(transactions.userID, ticket));

    await TransactionServices.saveTransaction(transactions);
  }

  Future<void> processingTopUp() async {}
}
