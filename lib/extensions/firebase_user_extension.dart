part of 'extensions.dart';

extension FirebaseUserExtension on FirebaseUser {
  User convertToUser(
          {String name = "no name",
          List<String> selectedGenres,
          String selectedLanguage,
          int balances = 0}) =>
      User(this.uid, this.email,
          name: name,
          selectedGenres: selectedGenres,
          selectedLanguage: selectedLanguage,
          balances: balances);

  Future<User> fromFireStore() async => await UserServices.getUser(this.uid);
}
