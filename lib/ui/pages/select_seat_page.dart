part of 'pages.dart';

class SelectSeatPage extends StatefulWidget {
  final Ticket ticket;

  const SelectSeatPage(this.ticket);

  @override
  _SelectSeatPageState createState() => _SelectSeatPageState();
}

class _SelectSeatPageState extends State<SelectSeatPage> {
  List<String> selectedSeats = [];
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context
            .bloc<PageBloc>()
            .add(GoToSelectShedulePage(widget.ticket.movieDetail));
        return;
      },
      child: Scaffold(
          body: Stack(children: [
        Container(
          color: accent1,
        ),
        SafeArea(
          child: Container(
            color: Colors.white,
          ),
        ),
        ListView(
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 22),
              height: 56,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.all(margin),
                      child: GestureDetector(
                        onTap: () {
                          context.bloc<PageBloc>().add(
                              GoToSelectShedulePage(widget.ticket.movieDetail));
                        },
                        child: Icon(Icons.arrow_back, color: Colors.black),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20, right: margin),
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 16),
                            width: MediaQuery.of(context).size.width / 2,
                            child: Text(
                              widget.ticket.movieDetail.title,
                              style: blackTextFont.copyWith(fontSize: 16),
                              maxLines: 2,
                              overflow: TextOverflow.clip,
                              textAlign: TextAlign.end,
                            ),
                          ),
                          Container(
                            width: 60,
                            height: 60,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: NetworkImage(imageBaseURL +
                                        'w154' +
                                        widget.ticket.movieDetail.posterPath))),
                          )
                        ],
                      ),
                    ),
                  ]),
            ),
            Container(
              margin: EdgeInsets.only(top: 30),
              width: 277,
              height: 84,
              decoration: BoxDecoration(
                  image:
                      DecorationImage(image: AssetImage('assets/screen.png'))),
            ),
            generateSeats(),
            BlocBuilder<UserBloc, UserState>(
              builder: (_, userState) => FloatingActionButton(
                elevation: 0,
                child: Icon(
                  Icons.arrow_forward,
                  color:
                      selectedSeats.length > 0 ? Colors.white : Colors.black12,
                ),
                backgroundColor:
                    selectedSeats.length > 0 ? mainColor : Colors.black12,
                onPressed: selectedSeats.length > 0
                    ? () async {
                        context.bloc<PageBloc>().add(GoToCheckoutPage(
                            widget.ticket.copyWith(seats: selectedSeats, totalPrice:20000)));
                      }
                    : null,
              ),
            ),
          ],
        ),
      ])),
    );
  }

  Column generateSeats() {
    List<int> numberOfSeats = [3, 5, 5, 5, 5];
    List<Widget> widgets = [];
    for (int i = 0; i < numberOfSeats.length; i++) {
      widgets.add(Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate(
            numberOfSeats[i],
            (index) => Padding(
              padding: EdgeInsets.only(
                  right: index < numberOfSeats[i] - 1 ? 16 : 0, bottom: 16),
              child: SelectableBox("${String.fromCharCode(i + 65)}${index + 1}",
                  width: 40,
                  height: 40,
                  textStyle: whiteNumberFont.copyWith(color: Colors.black),
                  isEnabled: index != 0,
                  isSelected: selectedSeats.contains(
                      "${String.fromCharCode(i + 65)}${index + 1}"), onTap: () {
                String seatNumber =
                    "${String.fromCharCode(i + 65)}${index + 1}";
                setState(() {
                  if (selectedSeats.contains(seatNumber)) {
                    selectedSeats.remove(seatNumber);
                  } else {
                    selectedSeats.add(seatNumber);
                  }
                });
              }),
            ),
          )));
    }

    return Column(children: widgets);
  }
}
