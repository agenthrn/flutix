part of 'widgets.dart';

class BrowseButton extends StatelessWidget {
  final String genre;

  const BrowseButton(this.genre);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 50,
          width: 50,
          margin: EdgeInsets.only(bottom:4),
          padding: EdgeInsets.all(7),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Color(0xFFEBEFF6),
            image: DecorationImage(
                image: AssetImage(("assets/ic_$genre.png").toLowerCase()),
                ),
          ),
        ),
        Center(
          child: Text(genre, style: blackTextFont.copyWith(fontSize: 12, fontWeight: FontWeight.w400),),
        )
      ],
    );
  }
}
