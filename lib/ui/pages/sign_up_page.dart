part of "pages.dart";

class SignUpPage extends StatefulWidget {
  final RegistrationData registrationData;

  SignUpPage(this.registrationData);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController retypePasswordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    nameController.text = widget.registrationData.name;
    emailController.text = widget.registrationData.email;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context.bloc<PageBloc>().add(GoToSplashPage());
      },
      child: Scaffold(
        body: Container(
          color: Colors.white,
          padding: EdgeInsets.symmetric(horizontal: margin),
          child: ListView(
            children: [
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: 22),
                    height: 56,
                    child: Stack(children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: GestureDetector(
                          onTap: () {
                            context.bloc<PageBloc>().add(GoToSplashPage());
                          },
                          child: Icon(Icons.arrow_back, color: Colors.black),
                        ),
                      ),
                      Center(
                        child: Text(
                          "Buat Akun",
                          style: blackTextFont.copyWith(fontSize: 20),
                        ),
                      )
                    ]),
                  ),
                  Container(
                    width: 90,
                    height: 104,
                    child: Stack(
                      children: [
                        Container(
                          height: 90,
                          width: 90,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: widget.registrationData.profileImage ==
                                          null
                                      ? AssetImage("assets/user_pic.png")
                                      : FileImage(widget
                                          .registrationData.profileImage))),
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: GestureDetector(
                            onTap: () async {
                              if (widget.registrationData.profileImage ==
                                  null) {
                                widget.registrationData.profileImage =
                                    await getImage();
                              } else {
                                widget.registrationData.profileImage = null;
                              }

                              setState(() {});
                            },
                            child: Container(
                              height: 28,
                              width: 28,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: AssetImage(widget.registrationData
                                                  .profileImage ==
                                              null
                                          ? "assets/btn_add_photo.png"
                                          : "assets/btn_delete_photo.png"))),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 36,
                  ),
                  TextField(
                      controller: nameController,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                          labelText: "Nama Lengkap",
                          hintText: "Nama Lengkap")),
                  SizedBox(
                    height: 16,
                  ),
                  TextField(
                      controller: emailController,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                          labelText: "Email",
                          hintText: "Email")),
                  SizedBox(
                    height: 16,
                  ),
                  TextField(
                      controller: passwordController,
                      obscureText: true,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                          labelText: "Password Anda",
                          hintText: "Password Anda")),
                  SizedBox(
                    height: 16,
                  ),
                  TextField(
                      controller: retypePasswordController,
                      obscureText: true,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                          labelText: "Ulangi Password Anda",
                          hintText: "Ulangi Password Anda")),
                  SizedBox(
                    height: 30,
                  ),
                  FloatingActionButton(
                    child: Icon(Icons.arrow_forward),
                    backgroundColor: mainColor,
                    elevation: 0,
                    onPressed: () {
                      if (!(nameController.text.trim() != "" &&
                          emailController.text.trim() != "" &&
                          passwordController.text.trim() != "" &&
                          retypePasswordController.text.trim() != "")) {
                        Flushbar(
                          backgroundColor: Colors.redAccent,
                          duration: Duration(milliseconds: 2000),
                          flushbarPosition: FlushbarPosition.TOP,
                          message: "Pastikan semua data telah terisi",
                        )..show(context);
                      } else if (passwordController.text.trim() !=
                          retypePasswordController.text.trim()) {
                        Flushbar(
                          backgroundColor: Colors.redAccent,
                          duration: Duration(milliseconds: 2000),
                          flushbarPosition: FlushbarPosition.TOP,
                          message: "Password tidak sama",
                        )..show(context);
                      } else if (passwordController.text.length < 6) {
                        Flushbar(
                          backgroundColor: Colors.redAccent,
                          duration: Duration(milliseconds: 2000),
                          flushbarPosition: FlushbarPosition.TOP,
                          message: "Password harus lebih dari 6 karakter",
                        )..show(context);
                      } else if (!EmailValidator.validate(
                          emailController.text)) {
                        Flushbar(
                          backgroundColor: Colors.redAccent,
                          duration: Duration(milliseconds: 2000),
                          flushbarPosition: FlushbarPosition.TOP,
                          message: "Password tidak sama",
                        )..show(context);
                      }else{
                        widget.registrationData.name = nameController.text;
                        widget.registrationData.email = emailController.text;
                        widget.registrationData.password = passwordController.text;

                        context.bloc<PageBloc>().add(GoToPreferencePage(widget.registrationData));
                      }
                    },
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
