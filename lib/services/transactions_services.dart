part of 'services.dart';

class TransactionServices {
  static CollectionReference transactionCollection =
      Firestore.instance.collection("transactions");
  static Future<void> saveTransaction(Transactions transactions) async {
    await transactionCollection.document().setData({
      'userID': transactions.userID,
      'title': transactions.title,
      'subtitle': transactions.subtitle,
      'time': transactions.time.millisecondsSinceEpoch,
      'amount': transactions.amount,
      'picture': transactions.picture
    });
  }

  static Future<List<Transactions>> getTransaction(String userID) async {
    QuerySnapshot snapshot = await transactionCollection.getDocuments();

    var documents = snapshot.documents.where((document)=>document.data['userID']==userID);
    return documents.map((e)=> Transactions(
      userID: e.data['userID'],
      title: e.data['title'],
      subtitle: e.data['subtitle'],
      time: DateTime.fromMillisecondsSinceEpoch(e.data['time']),
      amount: e.data['amount'],
      picture: e.data['picture'],
    )).toList();
  }
}
