part of 'pages.dart';

class MoviePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: ListView(
        padding: EdgeInsets.only(bottom: 100),
        children: [
          Container(
            decoration: BoxDecoration(
              color: accent1,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(20),
                bottomRight: Radius.circular(20),
              ),
            ),
            padding: EdgeInsets.fromLTRB(margin, 20, margin, 30),
            child: BlocBuilder<UserBloc, UserState>(
              builder: (_, userState) {
                if (userState is UserLoaded) {
                  if (imageFileToUpload != null) {
                    uploadImage(imageFileToUpload).then((imageUrl) {
                      imageFileToUpload = null;
                      context
                          .bloc<UserBloc>()
                          .add(UpdateData(profileImage: imageUrl));
                    });
                  }
                  return Row(
                    children: [
                      Container(
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.grey, width: 1),
                        ),
                        child: Stack(
                          children: [
                            SpinKitFadingCircle(color: accent2, size: 50),
                            Container(
                              width: 50,
                              height: 50,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: (userState.user.profilePicture ==
                                              ""
                                          ? AssetImage("assets/user_pic.png")
                                          : NetworkImage(
                                              userState.user.profilePicture)))),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                              width: MediaQuery.of(context).size.width -
                                  2 * margin -
                                  78,
                              child: Text(
                                userState.user.name,
                                style: whiteTextFont.copyWith(fontSize: 18),
                                maxLines: 1,
                                overflow: TextOverflow.clip,
                              )),
                          Text(
                              NumberFormat.currency(
                                      locale: "id_ID",
                                      decimalDigits: 0,
                                      symbol: "IDR ")
                                  .format(userState.user.balances ?? 0),
                              style: yellowNumberFont.copyWith(fontSize: 14))
                        ],
                      )
                    ],
                  );
                } else {
                  return SpinKitFadingCircle(
                    color: accent2,
                    size: 50,
                  );
                }
              },
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(margin, 30, margin, 12),
            child: Text(
              "Sedang Tayang",
              style: blackTextFont.copyWith(
                  fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 140,
            child: BlocBuilder<MovieBloc, MovieState>(builder: (_, movieState) {
              if (movieState is MovieLoaded) {
                List<Movie> movies = movieState.movies.sublist(0, 10);
                return ListView.builder(
                    itemCount: movies.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (_, index) => Container(
                        margin: EdgeInsets.only(
                            left: (index == 0) ? margin : 0,
                            right: (index < movies.length - 1) ? 5 : 5),
                        child: MovieCard(movies[index], onTap: () {
                          context
                              .bloc<PageBloc>()
                              .add(GoToMovieDetailPage(movies[index]));
                        })));
              } else {
                return SpinKitFadingCircle(color: mainColor, size: 50);
              }
            }),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(margin, 30, margin, 12),
            child: Text(
              "Cari Film",
              style: blackTextFont.copyWith(
                  fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          BlocBuilder<UserBloc, UserState>(builder: (_, userState) {
            if (userState is UserLoaded) {
              return Container(
                margin: EdgeInsets.symmetric(horizontal: margin),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: List.generate(
                        userState.user.selectedGenres.length,
                        (index) => BrowseButton(
                            userState.user.selectedGenres[index]))),
              );
            } else {
              return SpinKitFadingCircle(color: mainColor, size: 50);
            }
          }),
          Container(
            margin: EdgeInsets.fromLTRB(margin, 30, margin, 12),
            child: Text(
              "Coming Soon",
              style: blackTextFont.copyWith(
                  fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 140,
            child: BlocBuilder<MovieBloc, MovieState>(builder: (_, movieState) {
              if (movieState is MovieLoaded) {
                List<Movie> movies = movieState.movies.sublist(10, 20);
                return ListView.builder(
                    itemCount: movies.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (_, index) => Container(
                        margin: EdgeInsets.only(
                            left: (index == 0) ? margin : 0,
                            right: (index < movies.length - 1) ? 5 : 5),
                        child: ComingSoonCard(movies[index])));
              } else {
                return SpinKitFadingCircle(color: mainColor, size: 50);
              }
            }),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(margin, 30, margin, 12),
            child: Text(
              "Get Lucky Day",
              style: blackTextFont.copyWith(
                  fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          Column(
            children: dummy
                .map((e) => Padding(
                      padding: EdgeInsets.fromLTRB(margin, 0, margin, 16),
                      child: PromoCard(e),
                    ))
                .toList(),
          )
        ],
      ),
    );
  }
}
