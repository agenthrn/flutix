part of "pages.dart";

class AccountConfirmationPage extends StatefulWidget {
  final RegistrationData registrationData;

  AccountConfirmationPage(this.registrationData);

  @override
  _AccountConfirmationPageState createState() =>
      _AccountConfirmationPageState();
}

class _AccountConfirmationPageState extends State<AccountConfirmationPage> {
  bool isSignUp = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context
            .bloc<PageBloc>()
            .add(GoToPreferencePage(widget.registrationData));
      },
      child: Scaffold(
        body: Container(
          color: Colors.white,
          padding: EdgeInsets.symmetric(horizontal: margin),
          child: ListView(
            children: [
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: 22),
                    height: 56,
                    child: Stack(children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: GestureDetector(
                          onTap: () {
                            context.bloc<PageBloc>().add(
                                GoToPreferencePage(widget.registrationData));
                          },
                          child: Icon(Icons.arrow_back, color: Colors.black),
                        ),
                      ),
                      Center(
                        child: Text(
                          "Konfirmasi Akun",
                          style: blackTextFont.copyWith(fontSize: 20),
                        ),
                      ),
                    ]),
                  ),
                  Container(
                      width: 150,
                      height: 150,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image:
                                (widget.registrationData.profileImage == null)
                                    ? AssetImage("assets/user_pic.png")
                                    : FileImage(
                                        widget.registrationData.profileImage)),
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Text("Halo",
                      style: blackTextFont.copyWith(
                          fontSize: 16, fontWeight: FontWeight.w300)),
                  Text("${widget.registrationData.name}",
                      style: blackTextFont.copyWith(
                          fontSize: 20, fontWeight: FontWeight.bold)),
                  SizedBox(
                    height: 110,
                  ),
                  (isSignUp == true)
                      ? SpinKitFadingCircle(color: mainColor, size: 45)
                      : SizedBox(
                          width: 250,
                          height: 45,
                          child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                              ),
                              color: mainColor,
                              child: Text(
                                "Buat Akun Saya",
                                style: whiteTextFont.copyWith(fontSize: 16),
                              ),
                              onPressed: () async {
                                setState(() {
                                  isSignUp = true;
                                });

                                imageFileToUpload =
                                    widget.registrationData.profileImage;

                                SignInSignUpResult result =
                                    await AuthServices.SignUp(
                                        widget.registrationData.email,
                                        widget.registrationData.password,
                                        widget.registrationData.name,
                                        widget.registrationData.selectedGenres,
                                        widget
                                            .registrationData.selectedLanguage);

                                if (result.user == null) {
                                  setState(() {
                                    isSignUp = false;
                                  });
                                  Flushbar(
                                    backgroundColor: Colors.redAccent,
                                    duration: Duration(milliseconds: 2000),
                                    flushbarPosition: FlushbarPosition.TOP,
                                    message: result.message,
                                  )..show(context);
                                }
                              }))
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
