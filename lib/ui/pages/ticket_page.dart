part of 'pages.dart';

class TicketPage extends StatefulWidget {
  @override
  _TicketPageState createState() => _TicketPageState();
}

class _TicketPageState extends State<TicketPage> {
  bool isExpiredTicket = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          BlocBuilder<TicketBloc, TicketState>(
              builder: (_, ticketState) => Container(
                    margin: EdgeInsets.symmetric(horizontal: margin),
                    child: TicketViewer(isExpiredTicket == true
                        ? ticketState.tickets
                            .where((ticket) =>
                                ticket.time.isBefore(DateTime.now()))
                            .toList()
                        : ticketState.tickets
                            .where((ticket) =>
                                !ticket.time.isBefore(DateTime.now()))
                            .toList()),
                  )),
          Container(height: 113, color: accent1),
          SafeArea(
            child: ClipPath(
              clipper: HeaderClipper(),
              child: Container(
                height: 113,
                color: accent1,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                          margin: EdgeInsets.only(left: 24, bottom: 32),
                          child: Text("My Tickets",
                              style: whiteTextFont.copyWith(fontSize: 20))),
                      Row(
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                isExpiredTicket = !isExpiredTicket;
                              });
                            },
                            child: Column(
                              children: [
                                Text("Newest",
                                    style: whiteTextFont.copyWith(
                                        fontSize: 16,
                                        color: !isExpiredTicket
                                            ? Colors.white
                                            : Color(0xff6F678E))),
                                SizedBox(height: 15),
                                Container(
                                    height: 4,
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    color: !isExpiredTicket
                                        ? accent2
                                        : Colors.transparent)
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                isExpiredTicket = !isExpiredTicket;
                              });
                            },
                            child: Column(
                              children: [
                                Text("Oldest",
                                    style: whiteTextFont.copyWith(
                                        fontSize: 16,
                                        color: isExpiredTicket
                                            ? Colors.white
                                            : Color(0xff6F678E))),
                                SizedBox(height: 15),
                                Container(
                                    height: 4,
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    color: isExpiredTicket
                                        ? accent2
                                        : Colors.transparent)
                              ],
                            ),
                          ),
                        ],
                      ),
                    ]),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class HeaderClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0, size.height - 20);
    path.quadraticBezierTo(0, size.height, 20, size.height);
    path.lineTo(size.width - 20, size.height);
    path.quadraticBezierTo(
        size.width, size.height, size.width, size.height - 20);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;
}

class TicketViewer extends StatelessWidget {
  final List<Ticket> tickets;

  const TicketViewer(this.tickets);

  @override
  Widget build(BuildContext context) {
    var sortedTickets = tickets;
    sortedTickets
        .sort((ticket1, ticket2) => ticket1.time.compareTo(ticket2.time));
    return ListView.builder(
        itemCount: sortedTickets.length,
        itemBuilder: (_, index) => Container(
              margin: EdgeInsets.only(top: index == 0 ? 133 : 20),
              child: Row(
                children: [
                  Container(
                      width: 70,
                      height: 90,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          image: DecorationImage(
                              image: NetworkImage(imageBaseURL +
                                  'w500' +
                                  sortedTickets[index].movieDetail.posterPath),
                              fit: BoxFit.cover))),
                  Column()
                ],
              ),
            ));
  }
}
