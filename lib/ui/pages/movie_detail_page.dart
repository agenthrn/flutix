part of 'pages.dart';

class MovieDetailPage extends StatelessWidget {
  final Movie movie;

  const MovieDetailPage(this.movie);

  @override
  Widget build(BuildContext context) {
    MovieDetail movieDetail;
    List<Credits> credits;
    return WillPopScope(
      onWillPop: () async {
        context.bloc<PageBloc>().add(GoToMainPage());
      },
      child: Scaffold(
        body: FutureBuilder(
          future: MovieServices.getMovieDetail(movie),
          builder: (_, snapshot) {
            if (snapshot.hasData) {
              movieDetail = snapshot.data;
              return FutureBuilder(
                future: MovieServices.getCredits(movie.id),
                builder: (_, snapshot) {
                  if (snapshot.hasData) {
                    credits = snapshot.data;
                    return Stack(
                      children: [
                        Container(
                          color: accent1,
                        ),
                        SafeArea(
                          child: Container(
                            color: Colors.white,
                          ),
                        ),
                        ListView(
                          children: [
                            Stack(
                              children: [
                                ShaderMask(
                                  shaderCallback: (rectangle) {
                                    return LinearGradient(
                                        begin: Alignment.topCenter,
                                        end: Alignment.bottomCenter,
                                        colors: [
                                          Colors.white.withOpacity(1),
                                          Colors.transparent
                                        ]).createShader(
                                        Rect.fromLTRB(0, 0, 0, 270));
                                  },
                                  blendMode: BlendMode.dstIn,
                                  child: Container(
                                    height: 270,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: NetworkImage(imageBaseURL +
                                                "w780" +
                                                movieDetail.posterPath),
                                            fit: BoxFit.cover)),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: GestureDetector(
                                    onTap: () {
                                      context
                                          .bloc<PageBloc>()
                                          .add(GoToMainPage());
                                    },
                                    child: Container(
                                        margin: EdgeInsets.all(margin),
                                        child: Icon(Icons.arrow_back,
                                            color: Colors.white)),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(movieDetail.title,
                                    style: blackTextFont.copyWith(
                                        fontSize: 24,
                                        fontWeight: FontWeight.w600),
                                    textAlign: TextAlign.center),
                                SizedBox(
                                  height: 6,
                                ),
                                Text(movieDetail.genresAndLanguage,
                                    style: grayTextFont.copyWith(fontSize: 12)),
                                SizedBox(
                                  height: 6,
                                ),
                                SizedBox(
                                  width: 150,
                                  child: RatingStars(
                                    voteAverage: movieDetail.rateAverage,
                                  ),
                                )
                              ],
                            ),
                            Container(
                              margin:
                                  EdgeInsets.fromLTRB(margin, 30, margin, 12),
                              child: Text(
                                "Cast & Crew",
                                style: blackTextFont.copyWith(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(
                              height: 120,
                              child: ListView.builder(
                                  itemCount: credits.length,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (_, index) => Container(
                                      margin: EdgeInsets.only(
                                          left: (index == 0) ? margin : 0,
                                          right: (index < credits.length - 1)
                                              ? 16
                                              : margin),
                                      child: CasterCard(credits[index]))),
                            ),
                            Container(
                              margin:
                                  EdgeInsets.fromLTRB(margin, 30, margin, 8),
                              child: Text(
                                "Storyline",
                                style: blackTextFont.copyWith(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              ),
                            ),
                            Container(
                              margin:
                                  EdgeInsets.fromLTRB(margin, 0, margin, 12),
                              child: Text(
                                "${movieDetail.desc}",
                                style: grayTextFont.copyWith(
                                    fontSize: 14, fontWeight: FontWeight.w400),
                              ),
                            ),
                            Container(
                              width: double.infinity,
                              height: 45,
                              margin: EdgeInsets.only(top: 30, bottom: 19),
                              padding: EdgeInsets.symmetric(horizontal: margin),
                              child: RaisedButton(
                                  child: Text("Continue to Book",
                                      style:
                                          whiteTextFont.copyWith(fontSize: 16)),
                                  color: mainColor,
                                  onPressed: () {
                                    context.bloc<PageBloc>().add(
                                        GoToSelectShedulePage(movieDetail));
                                  }),
                            ),
                          ],
                        ),
                      ],
                    );
                  } else {
                    return SpinKitFadingCircle(color: mainColor, size: 50);
                  }
                },
              );
            } else {
              return SpinKitFadingCircle(color: mainColor, size: 50);
            }
          },
        ),
      ),
    );
  }
}
