part of 'models.dart';

class Promo extends Equatable {
  final String title;
  final String subtitle;
  final int discount;

  Promo(
      {@required this.title, @required this.subtitle, @required this.discount});

  @override
  // TODO: implement props
  List<Object> get props => [title, subtitle, discount];
}

List<Promo> dummy = [
  Promo(
      title: "Promo Akhir Pekan",
      subtitle: "Promo khusus sabtu minggu 2%",
      discount: 2),
  Promo(
      title: "Promo Akhir Tahun",
      subtitle: "Promo khusus akhir tahun 2020 10%",
      discount: 10),
];
