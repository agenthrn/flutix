part of 'models.dart';

class Movie extends Equatable {
  final int id;
  final String title;
  final double rateAverage;
  final String desc;
  final String posterPath;
  final String backdropPath;

  Movie(
      {@required this.id,
      @required this.title,
      @required this.rateAverage,
      @required this.desc,
      @required this.posterPath,
      @required this.backdropPath});

  factory Movie.fromJSON(Map<String, dynamic> json)=> Movie(id: json['id'], desc: json['overview'], backdropPath: json['backdrop_path'], posterPath: json['poster_path'], rateAverage: (json['vote_average'] as num).toDouble(), title: json['title']);

  @override
  // TODO: implement props
  List<Object> get props =>
      [id, title, rateAverage, desc, posterPath, backdropPath];
}
