part of 'shared.dart';

const double margin = 24;
Color mainColor = Color(0xFF503E9D);
Color accent1 = Color(0xFF2c1f63);
Color accent2 = Color(0xFFfBD460);
Color accent3 = Color(0xFFADADAD);

TextStyle blackTextFont = GoogleFonts.raleway()
    .copyWith(color: Colors.black, fontWeight: FontWeight.bold);
TextStyle whiteTextFont = GoogleFonts.raleway()
    .copyWith(color: Colors.white, fontWeight: FontWeight.bold);
TextStyle purpleTextFont = GoogleFonts.raleway()
    .copyWith(color: mainColor, fontWeight: FontWeight.bold);
TextStyle grayTextFont =
    GoogleFonts.raleway().copyWith(color: accent3, fontWeight: FontWeight.bold);

TextStyle whiteNumberFont =
    GoogleFonts.openSans().copyWith(color: Colors.white);
TextStyle yellowNumberFont =
    GoogleFonts.openSans().copyWith(color: accent2);