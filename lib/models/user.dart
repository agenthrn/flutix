part of 'models.dart';

class User extends Equatable {
  final String uid;
  final String email;
  final String name;
  final String profilePicture;
  final List<String> selectedGenres;
  final String selectedLanguage;
  final int balances;

  User(this.uid, this.email,
      {this.name,
      this.profilePicture,
      this.selectedGenres,
      this.selectedLanguage,
      this.balances});

  User copyWith({String name, String profilePicture, int balance}) =>
      User(this.uid, this.email,
          name: name ?? this.name,
          profilePicture: profilePicture ?? this.profilePicture,
          balances: balance ?? this.balances,
          selectedLanguage: selectedLanguage,
          selectedGenres: selectedGenres);

  @override
  String toString() {
    return "[$uid] - $name, $email";
  }

  @override
  List<Object> get props => [
        uid,
        email,
        name,
        profilePicture,
        selectedGenres,
        selectedLanguage,
        balances
      ];
}
