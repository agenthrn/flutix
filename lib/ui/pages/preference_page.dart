part of "pages.dart";

class PreferencePage extends StatefulWidget {
  final RegistrationData registrationData;

  final List<String> genres = [
    "Horor",
    "Music",
    "Action",
    "Drama",
    "War",
    "Crime"
  ];
  final List<String> languages = ["Bahasa", "Korean", "English", "Japanese"];

  PreferencePage(this.registrationData);
  @override
  _PreferencePageState createState() => _PreferencePageState();
}

class _PreferencePageState extends State<PreferencePage> {
  List<String> selectedGenres = [];
  String selectedLanguage = "English";

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        widget.registrationData.password = '';
        context
            .bloc<PageBloc>()
            .add(GoToRegistrationPage(widget.registrationData));
      },
      child: Scaffold(
        body: Container(
          color: Colors.white,
          padding: EdgeInsets.symmetric(horizontal: margin),
          child: ListView(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 56,
                    margin: EdgeInsets.only(top: 20, bottom: 4),
                    child: GestureDetector(
                        onTap: () {
                          widget.registrationData.password = '';
                          context.bloc<PageBloc>().add(
                              GoToRegistrationPage(widget.registrationData));
                        },
                        child: Icon(Icons.arrow_back)),
                  ),
                  Text(
                    "Pilih Genre\nFavorit",
                    style: blackTextFont.copyWith(fontSize: 20),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Wrap(
                    spacing: 24,
                    runSpacing: 24,
                    children: generateGenreWidgets(context),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Text(
                    "Pilih Bahasa\nPilihan",
                    style: blackTextFont.copyWith(fontSize: 20),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Wrap(
                    spacing: 24,
                    runSpacing: 24,
                    children: generateLanguageWidgets(context),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Center(
                    child: FloatingActionButton(
                      child: Icon(Icons.arrow_forward),
                      backgroundColor: mainColor,
                      elevation: 0,
                      onPressed: () {
                        if (selectedGenres.length != 4) {
                          Flushbar(
                            backgroundColor: Colors.redAccent,
                            duration: Duration(milliseconds: 2000),
                            flushbarPosition: FlushbarPosition.TOP,
                            message: "Pastikan anda memilih 4 pilihan genre",
                          )..show(context);
                        }else{
                          widget.registrationData.selectedLanguage = selectedLanguage;
                          widget.registrationData.selectedGenres = selectedGenres;

                          context.bloc<PageBloc>().add(GoToConfirmationPage(widget.registrationData));
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> generateGenreWidgets(BuildContext context) {
    double width = (MediaQuery.of(context).size.width - 2 * margin - 24) / 2;

    return widget.genres
        .map((e) => SelectableBox(e,
                width: width,
                isSelected: selectedGenres.contains(e), onTap: () {
              onSelectGenre(e);
            }))
        .toList();
  }

  List<Widget> generateLanguageWidgets(BuildContext context) {
    double width = (MediaQuery.of(context).size.width - 2 * margin - 24) / 2;

    return widget.languages
        .map((e) => SelectableBox(e,
                width: width, isSelected: selectedLanguage == e, onTap: () {
              setState(() {
                selectedLanguage = e;
              });
            }))
        .toList();
  }

  void onSelectGenre(String genre) {
    if (selectedGenres.contains(genre)) {
      setState(() {
        selectedGenres.remove(genre);
      });
    } else {
      if (selectedGenres.length < 4) {
        setState(() {
          selectedGenres.add(genre);
        });
      }
    }
  }
}
