part of 'widgets.dart';

class CasterCard extends StatelessWidget {
  final Credits credits;

  const CasterCard(this.credits);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 80,
          width: 70,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            image: DecorationImage(
                image: NetworkImage(
                    imageBaseURL + "original" + credits.profilePath),
                fit: BoxFit.cover),
          ),
        ),
        SizedBox(
          width: 70,
          child: Center(
            child: Text(
              credits.name,
              textAlign: TextAlign.center,
              maxLines: 2,
              overflow: TextOverflow.clip,
              style: blackTextFont.copyWith(
                  fontSize: 12, fontWeight: FontWeight.w400),
            ),
          ),
        )
      ],
    );
  }
}
