part of 'models.dart';

class Ticket extends Equatable {
  final MovieDetail movieDetail;
  final Theater theater;
  final DateTime time;
  final String bookingCode;
  final List<String> seats;
  final String name;
  final int totalPrice;

  Ticket(this.movieDetail, this.theater, this.time, this.bookingCode,
      this.seats, this.name, this.totalPrice);

  @override
  List<Object> get props =>
      [movieDetail, theater, time, bookingCode, seats, name, totalPrice];

  Ticket copyWith({String name, String profilePicture, int balance, List<String> seats, int totalPrice}) => Ticket(
      movieDetail ?? this.movieDetail,
      theater ?? this.theater,
      time ?? this.time,
      bookingCode ?? this.bookingCode,
      seats ?? this.seats,
      name ?? this.name,
      totalPrice ?? this.totalPrice);

  String get seatsInString {
    String newSeat = '';
    for (var seat in seats) {
      newSeat += seat + ((seat != seats.last) ? ', ' : '');
    }
    return newSeat;
  }
}
