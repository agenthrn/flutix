part of 'pages.dart';

class CheckoutPage extends StatefulWidget {
  final Ticket ticket;

  CheckoutPage(this.ticket);

  @override
  _CheckoutPageState createState() => _CheckoutPageState();
}

class _CheckoutPageState extends State<CheckoutPage> {
  @override
  Widget build(BuildContext context) {
    int total = (widget.ticket.totalPrice + 1500) * widget.ticket.seats.length;
    return WillPopScope(
        onWillPop: () async {
          context.bloc<PageBloc>().add(GoToSelectSeatPage(widget.ticket));
        },
        child: Scaffold(
            body: Stack(
          children: [
            Container(
              color: accent1,
            ),
            SafeArea(
              child: Container(
                color: Colors.white,
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: margin, right: margin),
              child: BlocBuilder<UserBloc, UserState>(
                builder: (_, userState) {
                  User user = (userState as UserLoaded).user;

                  return ListView(
                    children: [
                      Column(children: [
                        Container(
                          margin: EdgeInsets.only(bottom: margin),
                          height: 56,
                          child: Stack(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: GestureDetector(
                                onTap: () {
                                  context
                                      .bloc<PageBloc>()
                                      .add(GoToSelectSeatPage(widget.ticket));
                                },
                                child:
                                    Icon(Icons.arrow_back, color: Colors.black),
                              ),
                            ),
                            Center(
                              child: Text(
                                "Checkout\nMovie",
                                style: blackTextFont.copyWith(fontSize: 20),
                                textAlign: TextAlign.center,
                              ),
                            )
                          ]),
                        ),
                        Row(
                          children: [
                            Container(
                              width: 70,
                              height: 90,
                              margin: EdgeInsets.only(right: margin),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  image: DecorationImage(
                                      image: NetworkImage(imageBaseURL +
                                          'w342' +
                                          widget.ticket.movieDetail.posterPath),
                                      fit: BoxFit.cover)),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                    width: MediaQuery.of(context).size.width -
                                        3 * margin -
                                        70,
                                    child: Text(widget.ticket.movieDetail.title,
                                        style: blackTextFont.copyWith(
                                            fontSize: 18),
                                        maxLines: 2,
                                        overflow: TextOverflow.clip)),
                                SizedBox(
                                    width: MediaQuery.of(context).size.width -
                                        3 * margin -
                                        70,
                                    child: Text(
                                        widget.ticket.movieDetail
                                            .genresAndLanguage,
                                        style: grayTextFont.copyWith(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w400),
                                        maxLines: 2,
                                        overflow: TextOverflow.clip)),
                                RatingStars(
                                  voteAverage:
                                      widget.ticket.movieDetail.rateAverage,
                                )
                              ],
                            )
                          ],
                        ),
                        Container(
                            margin: EdgeInsets.symmetric(vertical: margin),
                            child: Divider(
                              color: Colors.grey,
                              thickness: 1,
                            )),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "ID Order",
                              style: grayTextFont.copyWith(
                                  fontSize: 16, fontWeight: FontWeight.w400),
                            ),
                            Text(
                              widget.ticket.bookingCode,
                              style: blackTextFont.copyWith(
                                  fontSize: 16, fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Cinema",
                              style: grayTextFont.copyWith(
                                  fontSize: 16, fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width / 2,
                              child: Text(
                                widget.ticket.theater.name +
                                    " - " +
                                    widget.ticket.theater.city,
                                style: blackTextFont.copyWith(
                                    fontSize: 16, fontWeight: FontWeight.w400),
                                textAlign: TextAlign.end,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Date & Time",
                              style: grayTextFont.copyWith(
                                  fontSize: 16, fontWeight: FontWeight.w400),
                            ),
                            Text(
                              widget.ticket.time.dayAndTime,
                              style: blackTextFont.copyWith(
                                  fontSize: 16, fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Seat Number",
                              style: grayTextFont.copyWith(
                                  fontSize: 16, fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width / 2,
                              child: Text(
                                widget.ticket.seatsInString,
                                style: blackTextFont.copyWith(
                                    fontSize: 16, fontWeight: FontWeight.w400),
                                textAlign: TextAlign.end,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Price",
                              style: grayTextFont.copyWith(
                                  fontSize: 16, fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width / 2,
                              child: Text(
                                "Rp. " +
                                    widget.ticket.totalPrice.toString() +
                                    " X " +
                                    widget.ticket.seats.length.toString(),
                                style: blackTextFont.copyWith(
                                    fontSize: 16, fontWeight: FontWeight.w400),
                                textAlign: TextAlign.end,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Fee",
                              style: grayTextFont.copyWith(
                                  fontSize: 16, fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width / 2,
                              child: Text(
                                "Rp. 1500"
                                        " X " +
                                    widget.ticket.seats.length.toString(),
                                style: blackTextFont.copyWith(
                                    fontSize: 16, fontWeight: FontWeight.w400),
                                textAlign: TextAlign.end,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Total",
                              style: grayTextFont.copyWith(
                                  fontSize: 16, fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width / 2,
                              child: Text(
                                NumberFormat.currency(
                                        locale: 'id_ID',
                                        symbol: 'IDR',
                                        decimalDigits: 2)
                                    .format(total),
                                style: blackTextFont.copyWith(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                                textAlign: TextAlign.end,
                              ),
                            ),
                          ],
                        ),
                        Container(
                            margin: EdgeInsets.symmetric(vertical: margin),
                            child: Divider(
                              color: Colors.grey,
                              thickness: 1,
                            )),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Your Wallet",
                              style: grayTextFont.copyWith(
                                  fontSize: 16, fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width / 2,
                              child: Text(
                                NumberFormat.currency(
                                        locale: 'id_ID',
                                        symbol: 'IDR',
                                        decimalDigits: 2)
                                    .format(user.balances),
                                style: blackTextFont.copyWith(
                                    color: user.balances >= total
                                        ? Color(0xff3E9D9D)
                                        : Colors.redAccent,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                                textAlign: TextAlign.end,
                              ),
                            ),
                          ],
                        ),
                        Container(
                          width: double.infinity,
                          height: 45,
                          margin: EdgeInsets.only(top: 30, bottom: 19),
                          child: RaisedButton(
                              child: Text(
                                  user.balances >= total
                                      ? "Checkout Now"
                                      : "Top Up My Wallet",
                                  style: whiteTextFont.copyWith(fontSize: 16)),
                              color: user.balances >= total
                                  ? Color(0xff3E9D9D)
                                  : mainColor,
                              onPressed: () {
                                if (user.balances >= total) {
                                  Transactions transaction = Transactions(
                                      userID: user.uid,
                                      title: widget.ticket.movieDetail.title,
                                      subtitle: widget.ticket.theater.name,
                                      time: DateTime.now(),
                                      amount: -total,
                                      picture:
                                          widget.ticket.movieDetail.posterPath);
                                  context.bloc<PageBloc>().add(GoToSuccessPage(
                                      widget.ticket.copyWith(totalPrice: total),
                                      transaction));
                                } else {
                                  context
                                      .bloc<PageBloc>()
                                      .add(GoToSuccessPage(null, null));
                                }
                              }),
                        ),
                      ]),
                    ],
                  );
                },
              ),
            )
          ],
        )));
  }
}
