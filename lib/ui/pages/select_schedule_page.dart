part of 'pages.dart';

class SelectSchedulePage extends StatefulWidget {
  final MovieDetail movieDetail;

  const SelectSchedulePage(this.movieDetail);

  @override
  _SelectSchedulePageState createState() => _SelectSchedulePageState();
}

class _SelectSchedulePageState extends State<SelectSchedulePage> {
  List<DateTime> dates;
  DateTime selectedDate;
  int selectedTime;
  Theater selectedTheater;
  bool isValid = false;

  @override
  void initState() {
    super.initState();
    dates =
        List.generate(7, (index) => DateTime.now().add(Duration(days: index)));
    selectedDate = dates[0];
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context.bloc<PageBloc>().add(GoToMovieDetailPage(widget.movieDetail));
        return;
      },
      child: Scaffold(
        body: Stack(
          children: [
            Container(
              color: accent1,
            ),
            SafeArea(
              child: Container(
                color: Colors.white,
              ),
            ),
            ListView(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: GestureDetector(
                    onTap: () {
                      context
                          .bloc<PageBloc>()
                          .add(GoToMovieDetailPage(widget.movieDetail));
                    },
                    child: Container(
                        margin: EdgeInsets.all(margin),
                        child: Icon(Icons.arrow_back, color: Colors.black)),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(margin, 20, margin, 16),
                  child: Text(
                    "Pilih Tanggal",
                    style: blackTextFont.copyWith(fontSize: 20),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 24),
                  height: 90,
                  child: ListView.builder(
                      itemCount: dates.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (_, index) => Container(
                            margin: EdgeInsets.only(
                                left: (index == 0) ? margin : 0,
                                right:
                                    (index < dates.length - 1) ? 16 : margin),
                            child: DateCard(dates[index],
                                isSelected: selectedDate == dates[index],
                                onTap: () {
                              setState(() {
                                selectedDate = dates[index];
                                isValid = false;
                                selectedTime = null;
                              });
                            }),
                          )),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(margin, 20, margin, 16),
                  child: Text(
                    "Pilih Bioskop",
                    style: blackTextFont.copyWith(fontSize: 20),
                  ),
                ),
                generateTimeTable(),
                SizedBox(height: 10),
                BlocBuilder<UserBloc, UserState>(
                  builder: (_, userState) => FloatingActionButton(
                    elevation: 0,
                    child: Icon(
                      Icons.arrow_forward,
                      color: isValid ? Colors.white : Colors.black12,
                    ),
                    backgroundColor: isValid ? mainColor : Colors.black12,
                    onPressed: isValid
                        ? () async {
                            context.bloc<PageBloc>().add(GoToSelectSeatPage(
                                Ticket(
                                    widget.movieDetail,
                                    selectedTheater,
                                    DateTime(
                                        selectedDate.year,
                                        selectedDate.month,
                                        selectedDate.day,
                                        selectedTime),
                                    randomAlphaNumeric(12).toUpperCase(),
                                    null,
                                    (userState as UserLoaded).user.name,
                                    null)));
                          }
                        : null,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Column generateTimeTable() {
    List<int> schedule = List.generate(7, (index) => 10 + index * 2);
    List<Widget> widgets = [];

    for (var theater in dummyTheater) {
      widgets.add(Container(
          margin: EdgeInsets.fromLTRB(margin, 0, margin, 16),
          child: Text(theater.name + " - " + theater.city,
              style: blackTextFont.copyWith(fontSize: 16))));

      widgets.add(Container(
          height: 50,
          margin: EdgeInsets.only(bottom: 20),
          child: ListView.builder(
            itemCount: schedule.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (_, index) => Container(
              margin: EdgeInsets.only(
                  left: (index == 0) ? margin : 0,
                  right: (index < schedule.length - 1) ? 16 : margin),
              child: SelectableBox(
                "${schedule[index]}:00",
                height: 50,
                isSelected: selectedTheater == theater &&
                    selectedTime == schedule[index],
                isEnabled: schedule[index] > DateTime.now().hour ||
                    selectedDate.day != DateTime.now().day,
                onTap: () {
                  setState(() {
                    if (schedule[index] > DateTime.now().hour ||
                        selectedDate.day != DateTime.now().day) {
                      selectedTheater = theater;
                      selectedTime = schedule[index];
                      isValid = true;
                    }
                  });
                },
              ),
            ),
          )));
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: widgets,
    );
  }
}
