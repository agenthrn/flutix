part of "models.dart";

class MovieDetail extends Movie {
  final List<String> genres;
  final String language;

  MovieDetail(Movie movie, {this.genres, this.language})
      : super(
            id: movie.id,
            backdropPath: movie.backdropPath,
            desc: movie.desc,
            posterPath: movie.posterPath,
            rateAverage: movie.rateAverage,
            title: movie.title);

  String get genresAndLanguage {
    String stringGenres = "";
    for (var genre in genres) {
      stringGenres += genre + (genre != genres.last ? ', ' : '');
    }
    return "$stringGenres - $language";
  }

  @override
  List<Object> get props => super.props + [genres, language];
}
