part of 'models.dart';

class Theater extends Equatable {
  final String name;
  final String city;

  Theater({this.name, this.city});

  @override
  List<Object> get props => [name, city];
}

List<Theater> dummyTheater = [
  Theater(name: "XXI Hypermart", city: "Malang"),
  Theater(name: "Cinepolis Matos", city: "Malang"),
  Theater(name: "CGV", city: "Surabaya"),
];
